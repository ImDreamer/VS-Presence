﻿namespace VSRichPresence {
    internal class RichClass {
        public bool LoadOnStartUp { get; set; }
        public string ApplicationId { get; set; }
        public bool Secretmode { get; set; }
    }
}
