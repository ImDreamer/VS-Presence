﻿using System;
using System.ComponentModel.Design;
using System.IO;
using Microsoft.VisualStudio.Shell;
using Newtonsoft.Json;

namespace VSRichPresence {

    internal sealed class EnableOrDisableSecretMode {
        private const int CommandId = 4131;

        private static readonly Guid CommandSet = new Guid("9cead51b-d641-4969-a680-992f7edef830");

        private readonly Package _package;

        private EnableOrDisableSecretMode(Package package) {
            _package = package ?? throw new ArgumentNullException(nameof(package));

            if (!(ServiceProvider.GetService(typeof(IMenuCommandService)) is OleMenuCommandService commandService)
            ) return;
            CommandID menuCommandId = new CommandID(CommandSet, CommandId);
            MenuCommand menuItem = new MenuCommand(MenuItemCallback, menuCommandId);
            commandService.AddCommand(menuItem);
        }

        private IServiceProvider ServiceProvider => _package;

        public static void Initialize(Package package) => new EnableOrDisableSecretMode(package);

        private static void MenuItemCallback(object sender, EventArgs e) {
            EnableVSPresence.RichClass.Secretmode = !EnableVSPresence.RichClass.Secretmode;
            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence", "config.json")), JsonConvert.SerializeObject(EnableVSPresence.RichClass));
        }
    }
}
