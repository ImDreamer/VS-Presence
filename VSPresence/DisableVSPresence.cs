﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;

namespace VSRichPresence {

    internal sealed class DisableVSPresence {
        private const int CommandId = 4130;


        private static readonly Guid CommandSet = new Guid("9cead51b-d641-4969-a680-992f7edef830");

        private readonly Package _package;

        private DisableVSPresence(Package package) {
            _package = package ?? throw new ArgumentNullException(nameof(package));

            if (!(ServiceProvider.GetService(typeof(IMenuCommandService)) is OleMenuCommandService commandService)) return;
            CommandID menuCommandId = new CommandID(CommandSet, CommandId);
            MenuCommand menuItem = new MenuCommand(MenuItemCallback, menuCommandId);
            commandService.AddCommand(menuItem);
        }

        private IServiceProvider ServiceProvider => _package;

        public static void Initialize(Package package) => new DisableVSPresence(package);

        private static void MenuItemCallback(object sender, EventArgs e) {
            if (EnableVSPresence.Shutdown) return;
            DiscordRpc.RichPresence presence = new DiscordRpc.RichPresence {
                Details = "VSPresence is disabled but",
                State = "it is still showing on discord :sad:"
            };
            DiscordRpc.UpdatePresence(ref presence);
            EnableVSPresence.Shutdown = true;
            DiscordRpc.RunCallbacks();
            DiscordRpc.ClearPresence();
            DiscordRpc.Shutdown();
            DiscordRpc.RunCallbacks();
        }
    }
}
