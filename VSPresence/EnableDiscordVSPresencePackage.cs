﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Newtonsoft.Json;

namespace VSRichPresence {
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [ProvideAutoLoad(UIContextGuids80.SolutionExists)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class EnableDiscordVSPresencePackage : Package {
        private const string PackageGuidString = "b597e212-abb6-47c9-bfcd-a2f10725d6f8";

        #region Package Members


        protected override void Initialize() {
            EnableVSPresence.Initialize(this);
            base.Initialize();
            DisableVSPresence.Initialize(this);
            EnableOrDisableSecretMode.Initialize(this);
            LoadOnStartup.Initialize(this);
            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence"))) {
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence"));
            }
            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence", "config.json"))) {
                EnableVSPresence.RichClass = JsonConvert.DeserializeObject<RichClass>(File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence", "config.json")));
                if (EnableVSPresence.RichClass.LoadOnStartUp) {
                    EnableVSPresence.StartUp();
                }
            }
            else {
                EnableVSPresence.RichClass.ApplicationId = "395355431356071936";
                EnableVSPresence.RichClass.LoadOnStartUp = true;
                File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "VSPresence", "VS Presence", "config.json"), JsonConvert.SerializeObject(EnableVSPresence.RichClass));
                EnableVSPresence.StartUp();
            }
        }

        ~EnableDiscordVSPresencePackage() {
            DiscordRpc.RunCallbacks();
            DiscordRpc.ClearPresence();
            DiscordRpc.Shutdown();
            DiscordRpc.RunCallbacks();
            //EnableVSPresence.Shutdown = true;
        }
        #endregion
    }
}
